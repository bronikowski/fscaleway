import requests
import json

ACCOUNT_ENDPOINT = 'http://account.scaleway.com'
REGIONS = {
        'par1': 'https://cp-par1.scaleway.com',
        'ams1': 'https://cp-ams1.scaleway.com',
}

class FScalewayFailedRequest(Exception):
    pass

class ReadOnlyAPI:

    def __init__(self, api_token, region='ams1'):

        self.api_token = api_token
        self.base_url = REGIONS[region]
        self._results = []

        self.headers = {
                'X-Auth-Token': api_token,
                'Content-Type': 'application/json',
        }

    def all(self):

        response = requests.get(self.base_url + '/' + self.current_api, headers=self.headers)
        if response.status_code == 200:
            return response.json()[self.current_api]
        else:
            raise FScalewayFailedRequest(response)

    def filter_by(self, key, value):

        filtered = []

        if not self._results:
            self._results = self.all()

        for entry in self._results:

            if key in entry:

                if value in entry[key]: # make it fuzzy

                    filtered.append(entry)

        self._results = filtered
        return self

    def results(self):
        return self._results

class ReadWriteAPI(ReadOnlyAPI):

    def create(self, **kwargs):

        response = requests.post(self.base_url + '/' + self.current_api, data=json.dumps(kwargs), headers=self.headers)
        if response.status_code == 201:
            return response.json()[self.current_api[0:-1]]
        else:
            raise FScalewayFailedRequest(response)

    def delete(self, object_id):
        response = requests.delete(self.base_url + '/' + self.current_api + '/' + object_id, headers=self.headers)
        if response.status_code == 204:
            return True
        else:
            raise FScalewayFailedRequest(response)


class Images(ReadOnlyAPI):
    current_api = 'images'

class Organizations(ReadOnlyAPI):

    current_api = 'organizations'

    def __init__(self, api_token, **kwargs):
        
        self.api_token = api_token
        self.base_url = ACCOUNT_ENDPOINT

        self.headers = {
                'X-Auth-Token': api_token,
                'Content-Type': 'application/json',
        }

    def me(self):
        return [x['id'] for x in self.all()]

class Servers(ReadWriteAPI):

    current_api = 'servers'

    def action(self, server_id, action_string):

        command = {
                "action": action_string,
        }
        response = requests.post(self.base_url + '/' + self.current_api + '/' + server_id + '/action', data=json.dumps(command), headers=self.headers)
        if response.status_code == 202:
            return response.json()['task']
        else:
            raise FScalewayFailedRequest(response)
