#!/usr/bin/env python

from distutils.core import setup

setup(name='fscaleway',
      version='0.3',
      description='A dirty API to cloud.scaleway.com',
      author='Emil Oppeln-Bronikowski',
      author_email='emil@fuse.pl',
      url='https://bitbucket.org/bronikowski/fscaleway',
      packages=['fscaleway',],
      install_requires = ['requests', 'scp'],
     )
